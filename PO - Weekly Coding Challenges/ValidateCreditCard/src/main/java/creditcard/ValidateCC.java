package main.java.creditcard;

public class ValidateCC {

	public static boolean validateCreditCard(long ccNum){

        String myCC = Long.toString(ccNum);
        if (myCC.length() < 14 || myCC.length() > 19) {
        	System.out.println("Credit Card Number: " + myCC);
        	System.out.println("Invalid credit card number");
        	return false;
        }
        
        int count = 1;
        
        int sum = 0;
        
        for (int i = myCC.length() - 1; i > - 1; i--){
        	
            int addable = Character.getNumericValue(myCC.charAt(i));
            
            if (!Character.isDigit(myCC.charAt(i))){
            	System.out.println("Credit Card Number: " + myCC);
            	System.out.println("Invalid credit card number");
                return false;
            } else if (count % 2 == 0){
                addable *= 2;
                if (addable > 9){
                    addable -= 9;

                }
            }
            count++;
            sum += addable;
        }
        System.out.println("Credit Card Number: " + myCC);
        System.out.println("Vaid credit card number!");
        return sum % 10 == 0;
        
    }
}
