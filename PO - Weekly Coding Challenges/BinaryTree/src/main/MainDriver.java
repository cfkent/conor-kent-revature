package main;

public class MainDriver {

	public static void main(String[] args) {
		BalancedTree myTree = new BalancedTree();
		
        int arry[] = new int[]{1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21};
        int n = arry.length - 1;
        BalancedTree.Node root = myTree.arryBal(arry, 0, n);
        
        System.out.println("Balanced Binary Search Tree:");
        System.out.println();
        myTree.printNode(root);
	}

}
