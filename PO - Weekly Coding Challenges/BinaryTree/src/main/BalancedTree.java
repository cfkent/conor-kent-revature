package main;

public class BalancedTree {
	
	static Node root;

    class Node {
        int data;
        Node left, right;

        Node(int d) {
            data = d;
            left = right = null;
        }
    }
    
    Node arryBal(int arr[], int start, int end) {
        if (start > end) {
            return null;
        }

        int mid = (start + end) / 2;
        Node myNode = new Node(arr[mid]);
        myNode.left = arryBal(arr, start, mid - 1);
        myNode.right = arryBal(arr, mid + 1, end);

        return myNode;
    }
    
    void printNode(Node node) {
        if (node == null) {
            return;
        }
        System.out.print(node.data + " ");
        printNode(node.left);
        printNode(node.right);
    }
}