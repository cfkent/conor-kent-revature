package main;

public class Encryption {

	public static void main(String[] args) {
		
		System.out.print("Encrypted String: ");
        System.out.println(Encrypt("My unencrypted String!"));
        System.out.println();

        System.out.print("Encrypted number: ");
        System.out.println(octConverter(1936513));
      
        }
	
        //Rot13 encryption
        public static String Encrypt(String rotString) {
        	
            rotString = rotString.toLowerCase();
            
            StringBuilder newRotString = new StringBuilder();
            
            for(int i = 0; i < rotString.length(); i++){
            	
                char thisChar = rotString.charAt(i);
                
                if (thisChar >= 'a' && thisChar <= 'm') {
                    thisChar += 13;
                }
                else {
                    thisChar -= 13;
                }
                
                newRotString.append(thisChar);
            }
            return newRotString.toString();
        }
        
        //Oct converter
        public static String octConverter (int number) {
           
                return Integer.toOctalString(number);

        }
        
    }