package dao;

import model.Users;

public interface UsersDao {
	
	public boolean findUser(Users user);
	
	public int findUserId(Users user); 
	
	public int findUserRole(Users user);
}
