package dao;

import java.util.List;

import model.Reimbursement;

public interface ReimbursementDao {
	
	public List<Reimbursement> selectAllReimbursements();
	
//	public List<Reimbursement> selectReimbursementByName(Reimbursement reimb_id);
	public List<Reimbursement> selectReimbursementByName();
		
	public boolean addReimbursement(Reimbursement reimb);
	
	public boolean removeReimbursement(int reimb_id);
	
	public boolean updateReimbursement(Reimbursement reimb);

}
