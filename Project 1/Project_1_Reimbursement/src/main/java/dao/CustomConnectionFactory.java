package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

//The CustomConnectionFactory manufactures the connections that my various DAOs
//need to transfer data to the database
public class CustomConnectionFactory {
	
	static { //necessary to make JDBC work in a WAR project
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
        }
    }

	public static String url = "jdbc:postgresql://datachan.cakgjyxjg8ds.us-east-2.rds.amazonaws.com/project1";
	public static String username= "datachan";
	public static String password="Cfk2588*";
	
	public static Connection getConnection() throws SQLException{
		return DriverManager.getConnection(url, username, password);
	}

}

