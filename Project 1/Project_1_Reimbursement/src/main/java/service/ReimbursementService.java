package service;

import java.util.List;

import model.Reimbursement;

public interface ReimbursementService {
	
	//READ
	public List<Reimbursement> selectAllReimbursements();

	public List<Reimbursement> selectReimbursementByName();

	public boolean addReimbursement(Reimbursement reimb_id);

	public boolean removeReimbursement(Reimbursement reimb_id);

	public boolean updateReimbursement(Reimbursement reimb_id);

}
