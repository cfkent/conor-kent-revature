import React from 'react'

const CreatePost = () => {
    return (
        <div className='postcontainer' >
            <div className='PostHeader'><h2>Create a Post</h2></div>
            <div className='line'></div>
            <form>
            <div className='userInfo'><div className='imgprofile'></div><h4>Username</h4></div><br/>
            <textarea className='PostContent'>what is in your mind </textarea><br/>

            
            <label>Add image to your post</label><input type="file" id="fileinput" multiple="false" accept="image/*" /><br/>
            <input type='submit' value='Add Post' className='btn btn-block'/><br/>
            </form>
        </div>
    )
}

export default CreatePost
